var request = require('supertest');

var app = require('../app.js');

describe('GET /', function()
{
    it('Respond with Hola mundo!', function(done)
    {
        request(app).get('/').expect('Hola mundo!', done)
    })
})